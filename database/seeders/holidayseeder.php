<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\holiday;
class holidayseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $holidays = [
            ["day" => 1 , "month" => 1],
            ["day" => 6 , "month" => 1],
            ["day" => 23 , "month" => 3],
            ["day" => 9 , "month" => 4],
            ["day" => 10 , "month" => 4],
            ["day" => 1 , "month" => 5],
            ["day" => 25 , "month" => 5],
            ["day" => 15 , "month" => 6],
            ["day" => 22 , "month" => 6],
            ["day" => 29 , "month" => 6],
            ["day" => 20 , "month" => 7],
            ["day" => 7 , "month" => 8],
            ["day" => 17 , "month" => 8],
            ["day" => 12 , "month" => 10],
            ["day" => 2 , "month" => 11],
            ["day" => 16 , "month" => 11],
            ["day" => 8 ,  "month" => 12],
            ["day" => 25 , "month" => 12],

           

        ];

        foreach ($holidays as &$c) { 
            $entity = new holiday();
            $entity->day = $c["day"];
            $entity->month = $c["month"];
            $entity->save();
        }
    }
}
