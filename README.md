## Sobre el proyecto
Proyecto de prueba desarrollado en el framework Laravel de php.
En esta aplicacion hemos desarrollado un api que es consumida en el proyecto angular 
de la presente prueba
## Requisitos

Este proyecto esta desarrollado en la version mas reciente de laravel a la fecha de hoy  11 de diciembre de 2020, realizar las pruebas de este proyector es necesario tener un motor de base de datos, en nuestro caso usamos maria DB o Mysql.
Se debe instalar composer y posteriormente Laravel.
se debe copiar el archivo .env.example a .env y configurar la conexion a la Db en el archivo .env.
luego ejecutamos el comando [composer install] para hacer la instalacion de las dependencias.
Una ves la configuracion en la carpeta principal abrimos una consola y ejecutamos el comando [php artisan migrate --seed], el cual se encargara de crear el esquema de base de datos y de ingresar valores predeterminados.

El servidor lo levantamos con el comando [php artisan serve] y deberia funcionar todo correctamente
