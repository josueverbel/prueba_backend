<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryDates extends Model
{
    use HasFactory;
    protected $fillable = [
        'date', 'days','nextenabledday',
    ];
}
