<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\HistoryDates;
use Illuminate\Http\Request;

class HistoryDatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return 
        Db::table('history_dates')
        ->orderBy('id', 'desc')
        
        ->get();
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $v = Validator::make($request->All(), [
            'days'  => 'required|numeric',
            'date'     => 'required|date',
            'nextenabledday' => 'required|date'
        ]);
        if ($v->fails()) return response()->json(["errors" => $v->errors()], 400);
        try {
            $historyDates = new HistoryDates($request->all());
            $historyDates->save();
        }catch (\Exception $e) {
            return 
            response()->json(['Error to update coverpage: ' . $e->getMessage()], 400);
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HistoryDates  $historyDates
     * @return \Illuminate\Http\Response
     */
    public function show(HistoryDates $historyDates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HistoryDates  $historyDates
     * @return \Illuminate\Http\Response
     */
    public function edit(HistoryDates $historyDates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HistoryDates  $historyDates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HistoryDates $historyDates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HistoryDates  $historyDates
     * @return \Illuminate\Http\Response
     */
    public function destroy(HistoryDates $historyDates)
    {
        //
    }
}
